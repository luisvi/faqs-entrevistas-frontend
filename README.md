# FAQs entrevistas Frontend



# Links de interés para preparar preguntas técnicas


- "Cracking the front-end interview" -> Muy interesante, **repaso de conceptos** que pueden ser preguntados en una entrevista:

https://www.freecodecamp.org/news/cracking-the-front-end-interview-9a34cd46237/



- "Never Fail Tech Interview" -> Preguntas concretas Angular y frontend para preparar a fondo una entrevista.

https://www.fullstack.cafe/blog/front-end-developer-interview-questions



- "Front-end-Developer-Interview-Questions" -> Repo github con listado enorme de preguntas frontend.

https://github.com/h5bp/Front-end-Developer-Interview-Questions


# Links para practicar código


- Test de preguntas Angular -> Muy interesante, viene con "code snippets"!

https://www.bestinterviewquestion.com/angular-2-interview-questions#


- Hacer katas, desafíos de código JS y TS.

https://www.codewars.com/


# Gurús Angular

- John Papa -> Referencia en el mundo Angular. Famoso por sus guías de estilo. Conveniente seguir publicaciones

https://github.com/johnpapa/angular-styleguide

- Todd Motto -> Muy activo en el mundo Angular, una referencia que es conveniente seguir.

https://github.com/toddmotto


# Material, resúmenes

- Hoja resumen de Git con comandos más importantes resaltados en amarillo.

https://gitlab.com/luisvi/faqs-entrevistas-frontend/uploads/e006cd17efd4ce220f0d392e5d276637/Resumen_git.pdf

**Buscar en google**:

frontend technical interview

frontend technical interview examples 

frontend technical interview questions

